export const getWeatherData = async (zipCodes) => {
  let response = await fetch(
    `${import.meta.env.VITE_API_URL}/weather/${zipCodes.join(",")}`
  );
  response = await response.json();
  return response;
};
