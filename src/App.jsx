import { Flex, Row } from "antd";
import { WeatherDashboard } from "./components/WeatherDashboard";
import { ZipCodeForm } from "./components/ZipCodeForm";
import { ZipCodeList } from "./components/ZipCodeList";
import { ZipCodeProvider } from "./components/ZipCodeProvider";
function App() {
  return (
    <ZipCodeProvider>
      <Flex gap={20} vertical={true} align="center" style = {{
        margin : 50
      }}>
        <ZipCodeForm />
        <ZipCodeList />
        <WeatherDashboard />
      </Flex>
    </ZipCodeProvider>
  );
}

export default App;
