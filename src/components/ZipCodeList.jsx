import { useZipCodes } from "./ZipCodeProvider";
import { Space, Tag } from "antd";
export function ZipCodeList() {
  const { zipCodes, remove } = useZipCodes();
  
  return (
    <Space>
      {zipCodes.map((zipCode, index) => (
        <Tag key={index} closable={true} onClose={() => remove(zipCode)}>
          {zipCode}
        </Tag>
      ))}
    </Space>
  );
}
