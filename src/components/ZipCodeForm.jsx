import { Button, Form, InputNumber, message } from "antd";
import { useZipCodes } from "./ZipCodeProvider";

import { useForm } from "antd/es/form/Form";
export function ZipCodeForm() {
  const [form] = useForm();
  const { add, zipCodes } = useZipCodes();
  const onFinish = (values) => {
    if (zipCodes.includes(values.zipCode)) {
      message.error("Zipcode already exists in the watchlist");
    } else {
      add(values.zipCode);
      form.resetFields();
    }
  };
  return (
    <Form form={form} layout="inline" onFinish={onFinish}>
      <Form.Item
        name={"zipCode"}
        label="Zip Code"
        rules={[{ required: true, message: "Please enter the zip code" }]}
      >
        <InputNumber
          placeholder="Zip Code for the location"
          style={{
            width: 200,
          }}
        />
      </Form.Item>
      <Form.Item>
        <Button type="primary" htmlType="submit">
          Add to watchlist
        </Button>
      </Form.Item>
    </Form>
  );
}
