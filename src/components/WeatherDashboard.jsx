import { useEffect, useState } from "react";
import { useZipCodes } from "./ZipCodeProvider";
import { Col, Empty, Row, Spin, message } from "antd";
import { WeatherCard } from "./WeatherCard";
import { getWeatherData } from "../api";

export function WeatherDashboard() {
  const { zipCodes } = useZipCodes();
  const [weatherData, setWeatherData] = useState([]);
  const [spinning, setSpinning] = useState(false);
  useEffect(() => {
    (async () => {
      if (zipCodes.length > 0) {
        try {
          setSpinning(true);
          setWeatherData(await getWeatherData(zipCodes));
        } catch (e) {
          console.error("Error while fetching weather data", e);
          message.error(
            "There was an error while fetching the data, Please try again"
          );
        } finally {
          setSpinning(false);
        }
      }
    })();
  }, [zipCodes]);
  return (
    <Spin spinning={spinning}>
      <Row gutter={[6, 6]}>
        {weatherData?.length > 0 ? (
          weatherData.map((data, index) => (
            <Col key={index}>
              <WeatherCard {...data} />
            </Col>
          ))
        ) : (
          <Empty
            description={
              spinning
                ? "Please wait..."
                : "Add a zip code to watchlist to view weather for it"
            }
          />
        )}
      </Row>
    </Spin>
  );
}
