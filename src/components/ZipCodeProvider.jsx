import { createContext, useContext, useState } from "react";
const getZipCodesFromStorage = () => {
  try {
    return JSON.parse(localStorage.getItem("zipcodes")) || [];
  } catch (e) {
    console.warn("Error while reading zip codes from storage", e);
    return [];
  }
};

const addZipCodeToStorage = (zipCode) => {
  const zipCodes = getZipCodesFromStorage();
  zipCodes.push(zipCode);
  localStorage.setItem("zipcodes", JSON.stringify(zipCodes));
};

const removeZipCodeFromStorage = (zipCode) => {
  const zipCodes = getZipCodesFromStorage();
  zipCodes.splice(zipCodes.indexOf(zipCode), 1);
  localStorage.setItem("zipcodes", JSON.stringify(zipCodes));
};

const ZipCodeContext = createContext({
  zipCodes: [],
  add: () => {},
  remove: () => {},
});

export const ZipCodeProvider = ({ children }) => {
  const [zipCodes, setZipCodes] = useState(getZipCodesFromStorage());
  return (
    <ZipCodeContext.Provider
      value={{
        zipCodes,
        add: (zipCode) => {
          setZipCodes([...zipCodes, zipCode]);
          addZipCodeToStorage(zipCode);
        },
        remove: (zipCode) => {
          setZipCodes(zipCodes.filter((z) => z !== zipCode));
          removeZipCodeFromStorage(zipCode);
        },
      }}
    >
      {children}
    </ZipCodeContext.Provider>
  );
};

export const useZipCodes = () => {
  const context = useContext(ZipCodeContext);
  if (!context) {
    throw new Error("useZipCodes must be used within a ZipCodeProvider");
  }
  return context;
};
