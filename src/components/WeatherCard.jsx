import { Card, Flex, Statistic } from "antd";

export function WeatherCard({ name, weather, main, dt, zipcode }) {
  return (
    <Card
      bordered={true}
      title={`${name} (${zipcode})`}
      extra={new Date(dt * 1000).toLocaleTimeString()}
      hoverable={true}
      style={{
        minWidth: 400,
      }}
    >
      <Flex justify="space-between">
        <Card.Meta
          title={weather?.[0]?.main}
          description={weather?.[0].description}
          avatar={<img src={weather?.[0].icon} />}
        />
        <Statistic
          value={main.temp - 273}
          precision={2}
          valueStyle={{ color: "#3f8600" }}
          suffix="°C"          
          style={{
            alignSelf: "flex-end",
          }}
        />
      </Flex>
    </Card>
  );
}
